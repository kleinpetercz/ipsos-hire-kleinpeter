(function ($, undefined) {

	$.nette.ext('spinner', {
		init: function (x) {
		},
		start: function (jqXHR, settings) {
			if (undefined != settings.nette && settings.nette.el.closest('.ajax-wrapper').length) {
				this.spinner = this.createSpinner();
				this.spinner.appendTo(settings.nette.el.closest('.ajax-wrapper'));
				this.spinner.fadeIn(180);
			}
		},
		complete: function (jqXHR, status, settings) {

			// Hide ajax spinner
			if (null != this.spinner) {
				this.spinner.fadeOut(180);
			}

			// Reinit
            // $('[data-toggle="tooltip"]').tooltip();
		}
	}, {
		createSpinner: function () {
			var spinner = $('<div>');
			spinner.addClass('ajax-spinner');
			return spinner;
		},
		spinner: null
	});

    // Init AJAX simulation (ajax spinner) on link
    $(document).on('click', 'a.ajax-simulate', function () {
        var spinner = $('<div>');
        spinner.addClass('ajax-spinner');
        spinner.appendTo($(this).closest('.ajax-wrapper'));
        spinner.fadeIn(180);
    });

    // Init AJAX simulation (ajax spinner) on form
    $(document).on('submit', 'form.ajax-simulate', function () {
        var spinner = $('<div>');
        spinner.addClass('ajax-spinner');
        spinner.appendTo($(this).closest('.ajax-wrapper'));
        spinner.fadeIn(180);
    });

})(jQuery);
