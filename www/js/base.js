$(function () {

    // Init common dependencies
    $.nette.init();

});

$(document).ready(function (){

    // Init liveFormValidation for better forms
    LiveForm.setOptions({
        wait: 500,
        showValid: true,
        showMessageClassOnParent: 'form-control',
        controlErrorClass: 'is-invalid',
        controlValidClass: 'is-valid',
    });

});