Ipsos projekt
=================

Úplné zadání naleznete po úvodní stránce tohoto webu. 
Pro spuštění projektu je potřeba PHP 8 a novější

Instalace
------------

	composer install


Složky `temp/` a `log/` musí být zapisovatelné.
