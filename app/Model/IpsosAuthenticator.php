<?php

declare(strict_types=1);

namespace App\Model;

use Nette;
use Nette\Neon\Neon;
use Nette\Security\SimpleIdentity;

class IpsosAuthenticator implements Nette\Security\Authenticator
{
    const TESTUSERLOGIN = 'ipsos';

    protected array $userList = [];

    public function __construct()
    {
        $config = Neon::decodeFile(__DIR__ . '/../../config/common.neon');
        $this->userList = $config['security']['users'] ?? [];
    }

    public function authenticate(string $username, string $password): SimpleIdentity
    {
        if (!array_key_exists($username, $this->userList)) {
            throw new Nette\Security\AuthenticationException('User not found.');
        }

        if ($this->userList[$username]['password'] !== $password) {
            throw new Nette\Security\AuthenticationException('Invalid password.');
        }

        $userData = $this->userList[$username]['data'];
        return new SimpleIdentity(
            1,
            $this->userList[$username]['roles'],
            [
                'login' => $username,
                'name' => $userData['name'],
                'surname' => $userData['surname']
            ]
        );
    }
}
