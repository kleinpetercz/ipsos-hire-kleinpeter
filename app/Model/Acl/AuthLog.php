<?php

declare(strict_types=1);

namespace App\Model\Acl;

use Tracy\ILogger;

class AuthLog implements AuthLogInterface
{
    /** @var string Log filename */
    const LOGFILE = 'appauth';

    public function __construct(private ILogger $logger)
    {
    }

    /**
     * Return main data to log
     *
     * @info Tady si umim predstavit ukladat vice informace nez jen IP., ale pokud není zadání, nechci vymýšlet zbytečnosti
     * @return array{ip: string}
     */
    protected function getRequestData(): array
    {
        return [
            'ip' => $_SERVER['REMOTE_ADDR'] ?: '',
        ];
    }

    /**
     * Log login attempt
     *
     * @param string $login
     * @return AuthLogInterface
     */
    public function logLoginAttempt(string $login): AuthLogInterface
    {
        $this->logger->log('Login attempt: ' . $login . '. Data: ' . json_encode(self::getRequestData()['ip']), self::LOGFILE);
        return $this;
    }

    /**
     * Log successful login
     *
     * @param $login
     * @return AuthLogInterface
     */
    public function logSuccessfulLogin($login): AuthLogInterface
    {
        $this->logger->log('User successfully logged-in: ' . $login . '. Data: ' . json_encode(self::getRequestData()), self::LOGFILE);
        return $this;
    }
}

interface AuthLogInterface
{
    /**
     * Log login attempt
     *
     * @param string $login
     * @return AuthLogInterface
     */
    public function logLoginAttempt(string $login): AuthLogInterface;

    /**
     * Log successful login
     *
     * @param string $login
     * @return AuthLogInterface
     */
    public function logSuccessfulLogin(string $login): AuthLogInterface;
}