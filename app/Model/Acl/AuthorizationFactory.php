<?php

declare(strict_types=1);

namespace App\Model\Acl;

use Nette\Security\Permission;

class AuthorizationFactory
{
    public static function create(): Permission
    {
        $acl = new Permission();

        $acl->addRole('guest');
        $acl->addRole('user', 'guest');
        $acl->addRole('admin', 'user');

        $acl->addResource('Homepage:default');

        $acl->allow('guest', 'Homepage:default');

        return $acl;
    }
}
