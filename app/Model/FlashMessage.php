<?php

declare(strict_types=1);

namespace App\Model;

interface FlashMessage
{
    const SUCCESS = 'success';
    const DANGER = 'danger';
    const WARNING = 'warning';
    const INFO = 'info';
}

// @info Od PHP 8.1 asi idealni vyuziti pro Enum

/**
enum FlashMessage {

case SUCCESS = 'success';
case DANGER = 'danger';
case WARNING = 'warning';
case INFO = 'info';

}
 */