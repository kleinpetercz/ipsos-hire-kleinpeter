<?php

// @info V pripade vice presenteru pod prihlasenim by bylo vhodne pouzit nejaky privat modul nebo alespon napr PrivatePresenter.php, kde by byla logika zjisteni
// prihlaseni a od te by dedily private presentery

declare(strict_types=1);

namespace App\Presenters;

use Nette;

class InfoPresenter extends BasePresenter
{
    use Nette\SmartObject;

    public function startup()
    {
        // True tu redirect none-logged user to login page
        $redirectToLogin = false;

        // Check access
        if (!$this->getUser()->isLoggedIn()) {

            // @info Buď 404 nebo redirect na login page s backlinkem, ze kterého se potom vrátí zpět na tuto stránku (připraveno, stačí hodit $redirectToLogin = true :) )
            if ($redirectToLogin) {

                $this->redirect('Sign:', [
                    'backlink' => $this->storeRequest()
                ]);

            } else {

                $this->error($this->translator->translate('messages.acl.accessDenied'));

            }

        }

        parent::startup();
    }

    public function actionDefault()
    {
        $this->setPageInfo('Privatni zona');
    }
}
