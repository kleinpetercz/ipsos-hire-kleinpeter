<?php

// @info Preklady jsou pouzil opravdu jen ve formulari. Jinak typicky vsude pres DI nacist Contributte\Translation\Translator service a ->translate preklady

declare(strict_types=1);

namespace App\Presenters;

use App\Forms\SignFormFactory;
use Contributte;
use Nette;
use Nette\Application;
use Nette\DI\Attributes\Inject;

class BasePresenter extends Nette\Application\UI\Presenter
{
    #[Inject]
    public SignFormFactory $signFormFactory;

    #[Inject]
    public Contributte\Translation\Translator $translator;

    #[Inject]
    public Contributte\Translation\LocalesResolvers\Session $translatorResolver;

    public function startup()
    {
        parent::startup();

        // Process language / locale
        $this->setLanguage();
    }

    /**
     * Process language / locale
     */
    private function setLanguage(): void
    {
        // Set language
        if (!empty($this->translator->getLocale())) {
            $this->translatorResolver->setLocale($this->translator->getLocale());
        }

        // Default domain
        $this->translator->createPrefixedTranslator('messages');

        // Allowed languages to template
        $this->template->allowedLanguages = $this->translator->getAvailableLocales();
    }

    /**
     * Set main page information (title, h1, description, ...)
     * Used primary in template
     *
     * @param string $title
     * @param string|null $h1
     * @return BasePresenter
     */
    public function setPageInfo(string $title, string $h1 = null)
    {
        $this->template->pageInfo = [
            'title' => $title,
            'h1' => empty($h1) ? $title : $h1
        ];

        return $this;
    }

    /**
     * Process language switch
     *
     * @param $lang
     * @throws Application\AbortException
     */
    public function handleSwitchLang(string $lang)
    {
        $this->translatorResolver->setLocale($lang);
        $this->redirect('this');
    }
}
