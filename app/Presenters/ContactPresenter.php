<?php

declare(strict_types=1);

namespace App\Presenters;

use App\Forms;
use Nette;
use Nette\DI\Attributes\Inject;

class ContactPresenter extends BasePresenter
{
    #[Inject]
    public Forms\ContactFormFactory $contactFormFactory;

    public function actionDefault()
    {
        $this->setPageInfo('Kontakt');
    }

    public function createComponentContactForm(): Forms\ContactForm
    {
        // Init contact form
        $form = $this->contactFormFactory->create();

        // Show form data to template
        $form->onSuccess[] = function (Nette\Application\UI\Form $form, $values) {

            // @info Idealni by bylo po odeslani stranku prenacist (flash messages by se drzelo), jen by se data musela drzet v session,...)
            $this->template->formData = $values;

            if ($this->isAjax()) {
                $this->redrawControl('Content');
                $this->redrawControl('FlashMessages');
            }
        };

        return $form;
    }
}
