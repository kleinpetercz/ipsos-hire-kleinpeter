<?php

declare(strict_types=1);

namespace App\Presenters;

use App\Forms\SignFormFactory;
use Nette;
use Nette\DI\Attributes\Inject;

final class SignPresenter extends BasePresenter
{
    /** @persistent  */
    public $backlink;

    #[Inject]
    public SignFormFactory $signFormFactory;

    public function actionDefault()
    {
        $this->setPageInfo('Přihlášení');
    }

    public function createComponentSignForm(): Nette\Application\UI\Form
    {
        return $this->signFormFactory->create();
    }

    public function actionOut(): void
    {
        $this->getUser()->logout();
        $this->redirect('Homepage:');
    }
}
