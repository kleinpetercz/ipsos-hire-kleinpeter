<?php

declare(strict_types=1);

namespace App\Forms;

use App\Model\FlashMessage;
use App\Model\IpsosAuthenticator;
use Contributte\Translation\Translator;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Security\User;
use Nette\Utils\ArrayHash;

class ContactForm extends Control
{
    /** @var array Error callbacks */
    public array $onError;

    /** @var array Success callbacks */
    public array $onSuccess;

    public function __construct(private Translator $translator, private User $userService)
    {
    }

    public function createComponentContactForm(): Form
    {
        // Create form inputs
        $form = new Form();
        $form->setTranslator($this->translator);
        $form->addText('name', 'messages.form.contactForm.name')->setRequired('messages.form.contactForm.requiredInput.name');
        $form->addText('surname', 'messages.form.contactForm.surname')->setRequired('messages.form.contactForm.requiredInput.surname');
        $form->addText('subject', 'messages.form.contactForm.subject')->setRequired('messages.form.contactForm.requiredInput.subject');
        $form->addTextArea('message', 'messages.form.contactForm.text')->setRequired('messages.form.contactForm.requiredInput.text');
        $form->addSubmit('submit', 'messages.global.form.send');

        // Define form callbacks
        $form->onSuccess[] = [$this, 'contactFormSuccess'];
        $form->onError[] = [$this, 'contactFormError'];

        // Set default values
        if ($this->userService->isLoggedIn()) {

            // Name / surname for specified user
            if ($this->userService->getIdentity()?->login === IpsosAuthenticator::TESTUSERLOGIN) {
                $form->setDefaults([
                    'name' => $this->userService->getIdentity()->name,
                    'surname' => $this->userService->getIdentity()->surname
                ]);
            }
        }

        return $form;
    }

    /**
     * Contact form error handler
     *
     * @param Form $form
     * @param ArrayHash $values
     */
    public function contactFormError(Form $form): void
    {
        // Error flash message to template
        $presenter = $this->getPresenter();
        foreach ($form->getErrors() as $error) {
            $presenter?->flashMessage($error, FlashMessage::DANGER);
        }

        // Call next error callbacks
        $this->onError($form);
    }

    /**
     * Contact form success handler
     *
     * @param Form $form
     * @param ArrayHash $values
     */
    public function contactFormSuccess(Form $form, ArrayHash $values): void
    {
        // Flash message to template (@info Data do sablony jdou z success callbacku v presenteru)
        $presenter = $form->getPresenterIfExists();
        $presenter?->flashMessage('Děkujeme za zprávu. Budeme vás kontaktovat.', FlashMessage::SUCCESS);

        // Call next callbacks
        $this->onSuccess($form, $values);
    }

    /**
     * Render form
     */
    public function render()
    {
        $this->template->render(__DIR__ . '/templates/ContactForm/default.latte');
    }
}

interface ContactFormFactory
{
    public function create(): ContactForm;
}
