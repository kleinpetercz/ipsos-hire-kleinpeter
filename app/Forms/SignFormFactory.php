<?php

declare(strict_types=1);

namespace App\Forms;

use App\Model\Acl\AuthLogInterface;
use App\Model\FlashMessage;
use Nette\Application\UI\Form;
use Nette\Security\AuthenticationException;
use Nette\Security\User;
use Nette\Utils\ArrayHash;

class SignFormFactory
{

    public function __construct(
        private User $user,
        private AuthLogInterface $authLogService
    ) {
    }

    public function create(): Form
    {
        $form = new Form();

        $form->addText('username', 'Jméno')
            ->setRequired();

        $form->addPassword('password', 'Heslo')
            ->setRequired();

        $form->addSubmit('submit', 'Přihlásit se');

        $form->onSuccess[] = [$this, 'success'];

        return $form;
    }

    public function success(Form $form, ArrayHash $values): void
    {
        $presenter = $form->getPresenterIfExists();

        // Log attempt
        $this->authLogService->logLoginAttempt($values->offsetGet('username'));

        try {

            // Login
            $this->user->login($values->offsetGet('username'), $values->offsetGet('password'));

            // Log successful login
            $this->authLogService->logSuccessfulLogin($values->offsetGet('username'));

            // Flash message and redirect
            $presenter?->flashMessage('Přihlášení probehlo úspěšně', FlashMessage::SUCCESS);
            $presenter->restoreRequest((string)$presenter->getParameter('backlink'));
            $presenter?->redirect('Homepage:');

        } catch (AuthenticationException $exception) {

            // Some error - error message and redirect
            $presenter?->flashMessage($exception->getMessage(), FlashMessage::DANGER);
            $presenter?->redirect('this');

        }
    }
}
